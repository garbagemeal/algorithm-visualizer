[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)

# Algorithm visualizer

A C# winform app containing a collection of algorithms visualized.
This repo is also available on [GitHub](https://github.com/garbagemeal/algorithm-visualizer).


### Sorting:
Insertion, Selection, Bubble, Merge, Quick, Heap, Counting, Radix, Introspective.
### Searching:
Binary, Ternary, Exponential.
### Trees (visuals not yet supported):
Binary tree and BST, Depth/Breadth first traversals, Construction using given traversals(pre/post/level + in).
### Graph theory:
BFS, DFS, Connected components using DFS or a disjoint set, Prim's and Kruskal's MST, DFS based maze generator, Top sort DFS, Kahn's top sort, SSSP for DAG, Dijkstra's SSSP, Kosaraju's and Tarjan's strongly connected components, Bellman Ford's SSSP.

# Screens

![Graph visualizer](images/graph.PNG)
![Preset dialog](images/preset-dialog.PNG)
![Vertex rightclick menu](images/vertex-rightclick.PNG)
![Empty location rightclick menu](images/empty-location-rightclick.PNG)

![Maze visualizer](images/maze.PNG)

![Array visualizer](images/array.PNG)

# Setup

In order to make use of the 'Presets' window you will need to have the MySQL DB with the preset details.
Personally I used PHPMyAdmin. an export of the DB is available in 'AlgorithmVisualizer\SQL Files'.

# License

This content is released under the [MIT license](https://opensource.org/licenses/MIT).
