﻿using System;
using System.Windows.Forms;

namespace AlgorithmVisualizer.Forms.FDGV
{
	public partial class NewPresetDialog : Form
	{
		public static string presetName;

		public NewPresetDialog()
		{
			InitializeComponent();
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			// Assign name for preset
			presetName = textBoxName.Text;
		}
	}
}
