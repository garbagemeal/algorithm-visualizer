﻿using System;
using System.IO;

namespace AlgorithmVisualizer.Forms.FDGV
{
	public class Preset
	{
		// A class to represent a graph preset
		// The serial is the adjacency list of the graph as a string
		public const string DEFAULT_PRESET_IMG = @"\Forms\FDGV\PresetImgs\0.png";
		public int Id { get; private set; }
		public string Name { get; private set; }
		public string Serial { get; private set; }
		public string ImgDir { get; private set; }

		public Preset(string name, string serial) :
			this(-1, name, serial, null)
		{ }
		public Preset(int id, string name, string serial, string imgDir)
		{
			Id = id;
			Name = name;
			Serial = serial;
			ImgDir = imgDir;
		}

		public string GetAbsoluteDir()
		{
			// Get the project's root dir, i.e:
			// C:\Users\USERNAME\source\repos\AlgorithmVisualizer\AlgorithmVisualizer

			var rootDir = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName;
			Console.WriteLine("ROOT DIR INSIDE Prese.cs: \n" + rootDir);
			// Suffix ImgDir if valid path, otherwise DEFAULT_PRESET_IMG
			return rootDir + (File.Exists(rootDir + ImgDir) ? ImgDir : DEFAULT_PRESET_IMG);
		}
		public override string ToString()
		{
			return $"Id: {Id}, Name: {Name}\n" +
				   $"Serial:\n{Serial}\n" +
				   $"ImgDir: {ImgDir}\n";
		}

	}
}
