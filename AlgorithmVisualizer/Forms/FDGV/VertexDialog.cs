﻿using System;
using System.Windows.Forms;

namespace AlgorithmVisualizer.Forms.FDGV
{
	public partial class VertexDialog : Form
	{
		public static int id, data;

		public VertexDialog()
		{
			InitializeComponent();
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			try
			{
				id = Int32.Parse(textBoxID.Text);
				data = Int32.Parse(textBoxData.Text);
			}
			catch (FormatException)
			{
				// id of -1 denotes an invalid id
				id = -1;
			}
		}
	}
}
