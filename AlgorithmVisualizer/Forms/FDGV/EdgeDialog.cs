﻿using System;

using System.Windows.Forms;

namespace AlgorithmVisualizer.Forms.FDGV
{
	public partial class EdgeDialog : Form
	{
		public static int to, cost;
		// true for DLink(undirected edge), false for SLink(directed edge)
		public static bool mode;
		private bool costInvalid = false;

		public EdgeDialog(bool _costInvalid = false)
		{
			// If costInvalid is true then an edge is being removed and therefore the cost invalid
			InitializeComponent();
			costInvalid = _costInvalid;
			// Adjust window title according to op type
			Text = costInvalid ? "Remove an edge" : "Add an edge";
			if (costInvalid)
			{
				// Hide stuff related to cost
				lblCost.Visible = textBoxCost.Visible = false;
			}
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			try
			{
				to = Int32.Parse(textBoxTo.Text);
				if(!costInvalid) cost = Int32.Parse(textBoxCost.Text);
				mode = radioBtnUndirected.Checked;
			}
			catch (FormatException)
			{
				// -1 denotes an invalid node id
				to = -1;
			}
		}
	}
}
