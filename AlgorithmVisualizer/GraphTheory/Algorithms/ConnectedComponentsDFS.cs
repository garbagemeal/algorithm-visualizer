﻿using System;
using System.Collections.Generic;
using System.Drawing;

using AlgorithmVisualizer.GraphTheory.FDGV;
using AlgorithmVisualizer.GraphTheory.Utils;

namespace AlgorithmVisualizer.GraphTheory.Algorithms
{
	class ConnectedComponentsDFS : GraphAlgorithm
	{
		
		public ConnectedComponentsDFS(GraphAdjList graph) : base(graph) => Solve();

		public override void Solve()
		{
			// List graph's components - setup
			// If the graph is not undirected does nothing
			if (GraphValidator.IsUndirected(graph))
			{
				List<GComponent> components = new List<GComponent>();

				HashSet<int> visited = new HashSet<int>();
				for (int nodeId = 0; nodeId < graph.NodeCount; nodeId++)
				{
					if (!visited.Contains(nodeId))
					{
						GComponent gComp = new GComponent();
						components.Add(gComp);
						Color color = gComp.Color;
						Solve(nodeId, gComp, visited, color);
						Sleep(1500);
					}
				}
				foreach (GComponent gComponent in components)
					Console.WriteLine(gComponent);
			}
		}
		private void Solve(int at, GComponent gComp, HashSet<int> visited, Color color)
		{
			// DFS to find the connected componenets
			visited.Add(at);
			gComp.NodeIds.Add(at);
			graph.DrawParticle(at, color);
			Sleep(1500);
			if (graph.AdjList[at] != null)
			{
				foreach (Edge edge in graph.AdjList[at])
				{
					if (!visited.Contains(edge.To))
					{
						// draw edge in orange to mark as visiting
						graph.RedrawSpring(edge, color, 0);
						Sleep(1000);
						Solve(edge.To, gComp, visited, color);
						// draw edge in dark gret to to mark as visited
						graph.RedrawSpring(edge, Colors.Visited);
						Sleep(1000);
					}
				}
			}
			Sleep(1000);
		}
	}
}
