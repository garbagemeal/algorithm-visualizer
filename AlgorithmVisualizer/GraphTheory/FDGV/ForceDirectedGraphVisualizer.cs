﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;

using AlgorithmVisualizer.GraphTheory.Utils;
using AlgorithmVisualizer.MathUtils;
using AlgorithmVisualizer.Threading;

namespace AlgorithmVisualizer.GraphTheory.FDGV
{
	public class ForceDirectedGraphVisualizer : PauseResume
	{
		/* Implementation of a force directed graph visualizing algorithm.

		* Multi-graph are unsupported, meaning that parallel edges may not exist.
		* i.e, the edges (0, 1, 5), (0, 1, 7) are parallel edges because both have the
		* same starting and ending nodes, only 1 of the above edges can exist at any
		* given moment in this graph.
		* Note that this property ensures that the graph's edges are unique!
		* 
		* Because of the limitation of not being a multi-graph, and in order to also
		* support "undirected" edges, If there exists and edge (u, v, x),
		* the edge (v, u, y) may exist IFF x = y
		* 
		* The pair of edges (u, v, x), (v, u, x) may be considered as a single undirected
		* edge in some of the algorithms implementations. */

		protected Dictionary<int, GNode> nodeLookup = new Dictionary<int, GNode>();
		private List<Particle> particles;
		private List<Spring> springs;

		// g is the graphics object of panelMain, gLog is a graphics object of panelLog.
		private Graphics g, gLog;
		public Graphics G { get { return g; } set{ g = value; } }
		public Graphics GLog { get { return gLog; } set { gLog = value; } }

		private int panelHeight, panelWidth;
		public int PanelHeight { set { panelHeight = Particle.PanelHeight = value; } }
		public int PanelWidth { set { panelWidth = Particle.PanelWidth = value; } }
		// Note that the condition: PARTICLE_SIZE <= PARTICLE_SPAWN_OFFSET must hold
		// for the prticles not to spawn outside of the panel (not clip ourside of it)
		protected const int PARTICLE_SIZE = 30, SPRING_REST_LEN = 125;
		private const int PARTICLE_SPAWN_OFFSET = 50;

		protected static Random rnd = new Random();
		
		// The delay factor for the sleep method, can range from 0-2 (0 is faster)
		protected double delayFactor = 1;
		public double DelayFactor
		{
			get { return delayFactor; }
			set { delayFactor = value; }
		}

		public ForceDirectedGraphVisualizer(Graphics gMain, Graphics _gLog, int _panelHeight, int _panelWidth)
		{
			g = gMain;
			gLog = _gLog;
			panelHeight = _panelHeight;
			panelWidth = _panelWidth;
			particles = new List<Particle>();
			springs = new List<Spring>();
			nodeLookup = new Dictionary<int, GNode>();

			// Used for the nodes not to position themselves outside of the panel
			Particle.PanelHeight = panelHeight;
			Particle.PanelWidth = panelWidth;
		}

		#region particle/spring list managment
		protected Vector GetRndPosVector()
		{
			// Note: the position vector returned will be within the panel and also offset
			// from borders by PARTICLE_SPAWN_OFFSET
			int x = rnd.Next(PARTICLE_SPAWN_OFFSET, panelWidth - PARTICLE_SPAWN_OFFSET);
			int y = rnd.Next(PARTICLE_SPAWN_OFFSET, panelHeight - PARTICLE_SPAWN_OFFSET);
			return new Vector(x, y);
		}

		public void AddParticle(Particle particle)
		{
			particles.Add(particle);
			particle.BoundWithinPanel();
		}
		public void RemParticle(int id)
		{
			// Locate particle via nodeLookup.
			// Note that upcasting is needed to be treated as a particle
			Particle particleToRem = ((Particle)nodeLookup[id]);
			particles.Remove(particleToRem);
			RemSpringsConnectedTo(id);
		}

		private Spring ToSpring(Edge edge, int dir = -1)
		{
			// Creates a Spring using the given edge (a clone)
			// Assuems edge.From and edge.To are valid node ids (part of the graph)
			Particle from = ((Particle)nodeLookup[edge.From]),
				to = ((Particle)nodeLookup[edge.To]);
			return new Spring(from, to, edge.Cost, SPRING_REST_LEN, dir);
		}
		private int FindSimillarSpringIdx(Spring searchSpring)
		{
			// Find and return the index of a spring similar to the given one; A spring is
			// similar to another IFF the composing node ids are the same, in any order.

			// Search for a similar spring, comaprison only via p1, p2 (node ids)
			for (int i = 0; i < springs.Count; i++)
				if (springs[i].SimilarTo(searchSpring)) return i;
			// Return -1 if no such spring exists. assuming given spring is of an edge
			// in the graph, will never return -1!
			return -1;
		}
		public void AddSpring(Spring spring)
		{
			// If adding a doubly linked spring there exists no similar spring in 'springs'
			if (spring.LinkDir != 2)
			{
				// Find similar springs index
				int similarSpringIdx = FindSimillarSpringIdx(spring);
				if (similarSpringIdx != -1)
				{
					// If similar spring found update it to become doubly linked. Can't be
					// a DLinked spring, or of the form(u, v, x), can only be of the form: (v, u, x)
					springs[similarSpringIdx].LinkDir = 2;
					return;
				}
			}
			// Similar spring not present in 'springs', simply add the new spring
			springs.Add(spring);
		}
		public void RemOrModSpring(Edge edge, bool DLinkedRemoval = false)
		{
			// Removes or modifies a spring in the graph, assuming given edge is valid.
			// Removal occours IFF spring's linkDir is 1 or linkDir is 2 and DLinkedRemoval = true
			// Note that its impossible that the edge is singly linked and DLinkedRemoval
			// = true, because we check before invoking this method that both edges exist
			Spring springToRemOrMod = ToSpring(edge);
			int similarSpringIdx = FindSimillarSpringIdx(springToRemOrMod);
			if (similarSpringIdx != -1)
			{
				Spring similarSpring = springs[similarSpringIdx];
				// Directed removal for an undirected edge(spring)
				if (similarSpring.LinkDir == 2 && !DLinkedRemoval)
					// If from = P1's id then linkDir is 1(p2 --> p1) else 0(p1 --> p2)
					similarSpring.LinkDir = similarSpring.P1.Id == edge.From ? 1 : 0;
				// All directions removed (can be 1 or 2)
				else springs.RemoveAt(similarSpringIdx);
			}
			else throw new ArgumentException("Given edge not present in graph!");
		}
		public void RemSpringsConnectedTo(int id)
		{
			// Remove any spring connected to a particle with the given id; that is a
			// particle p where p.P1.Id or p.P2.Id == id
			for (int i = 0; i < springs.Count; )
				if (springs[i].ContainsID(id)) springs.RemoveAt(i);
				else i++;
		}

		private void ClearSpringsAndParticles()
		{
			// Clears spring and particle lists
			springs.Clear();
			particles.Clear();
		}
		#endregion

		#region Visuals via panel
		private void ClearPanel() => g.Clear(Colors.Undraw);
		public void ClearVisualization()
		{
			// Clears the visuals and thier resources; the particle and spring lists.
			// Invoke this method after discarding the graph, i.e, the adjacency list.
			ClearSpringsAndParticles();
			ClearPanel();
		}
		public void DrawGraph()
		{
			// Draws the graph by first applying forces
			ClearPanel();
			ApplyForces();
			DrawSprings();
			DrawParticles();
		}
		private void ApplyForces()
		{
			foreach (Particle particle in particles)
			{
				// Pull this particle to the center
				particle.PullToCenter();
				// Repel all other particles via this particle
				particle.ApplyRepulsiveForces(particles);
			}
			// Apply forces on the particles via the springs
			foreach (Spring spring in springs) spring.ExertForcesOnParticles();

			// Update all particle positions
			foreach (Particle particle in particles) particle.UpdatePos();
		}
		public void DrawGraphForceless()
		{
			// Draw the graph without applying forces
			ClearPanel();
			DrawSprings();
			DrawParticles();
		}
		public void ResetGraphColorsAndDirs()
		{
			// Reset particle colors
			ResetParticleColors();
			// Reset linkDirState for springs
			foreach (var spring in springs)
			{
				spring.Undraw(g);
				spring.ResetLinkDirState();
				spring.Draw(g);
			}
			// Reset spring colors
			ResetSpringColors();
		}
		public void Visualize()
		{
			// Main method used to visualize the graph - Force directed graph drawing
			const int MAX_NUM_ITR = 750;
			const float EPSILON = 0.1f;
			int i = 0;
			// Run the FDGV as long i < MAX_NUM_ITR and
			// the maximal velocity for all particles per iteration > EPSILON
			do
			{
				Particle.MAX_VEL_MAG_PER_ITR = 0;
				DrawGraph();
				i++;
				// Check if pause event has been raised
				CheckForPause();
			} while (i < MAX_NUM_ITR && Particle.MAX_VEL_MAG_PER_ITR > EPSILON);

			// Reset max_vel/itr for next invocation of this method
			Particle.MAX_VEL_MAG_PER_ITR = 0;
		}

		// The 3 following methods assume that the given particle id exists
		public void DrawParticle(int id) => ((Particle)nodeLookup[id]).Draw(g);
		public void DrawParticle(int id, Color innerColor)
		{
			// Set given particle's innerColor and draw it
			((Particle)nodeLookup[id]).InnerColor = innerColor;
			DrawParticle(id);
		}
		public void DrawParticle(int id, Color innerColor, Color borderColor)
		{
			// Set given particle's innerColor and borderColor and draw it
			((Particle)nodeLookup[id]).InnerColor = innerColor;
			((Particle)nodeLookup[id]).BorderColor = borderColor;
			DrawParticle(id);
		}
		public void DrawParticles()
		{
			// Draw each particle
			foreach (Particle particle in particles) particle.Draw(g);
		}
		public void UndrawParticles()
		{
			// Unraw each particle
			foreach (Particle particle in particles) particle.Undraw(g);
		}
		public void RedrawParticles()
		{
			UndrawParticles();
			DrawParticles();
		}
		public void ResetParticleColor(int id)
		{
			((Particle)nodeLookup[id]).SetDefaultColors();
			DrawParticle(id);
		}
		public void ResetParticleColors()
		{
			foreach (var particle in particles)
			{
				particle.SetDefaultColors();
				particle.Draw(g);
			}
		}

		public void DrawSpring(Edge edge)
		{
			// Find the spring representing the edge; the similar spring.
			int similarSpringIdx = FindSimillarSpringIdx(ToSpring(edge));
			// Found a similar spring - draw it
			if (similarSpringIdx != -1) springs[similarSpringIdx].Draw(g);
			// Otherwise throw
			else throw new ArgumentException("Given edge not present in graph!");
		}
		public void ReverseSprings()
		{
			// Reverse LinkDirState foreach spring if LinkDirState != 2
			foreach (Spring spring in springs)
			{
				if (spring.LinkDirState != 2)
				{
					// Undraw original spring before modifying state
					spring.Undraw(g);
					// If LinkDirState is 0, set linkDirState to 1,
					// otherwise LinkDirState is 1, set linkDirState to 0.
					spring.LinkDirState = (spring.LinkDirState + 1) % 2;
					// Draw the spring to reflect the edge reversal (new state)
					spring.Draw(g);
				}
			}
		}
		public void RedrawSpring(Edge edge, Color color, int dir = -1)
		{
			/*
			 * Assumes given edge exists in the graph!
			 * Redraws the spring similar to the given edge in the spring list (springs)
			 * In either case will first find the spring and undraw it, then redraw the requested spring
			 *
			 * dir:
			 * -1 - redraw the original edge as is (stored in springs)
			 *  0 - redraw dricted edge from edge.From to edge.To
			 *  1 - redraw dricted edge from edge.To to edge.From
			 *  2 - redraw undicted edge from edge.From to edge.To 
			 *  
			 *  Assumption: both the composing nodes and the edge exist.
			 */

			// Find a similar spring
			Spring searchSpring = ToSpring(edge, dir);
			int similarSpringIdx = FindSimillarSpringIdx(searchSpring);
			if (similarSpringIdx != -1)
			{
				Spring similarSpring = springs[similarSpringIdx];
				// Undraw original spring (as it appeared in the graph)
				similarSpring.Undraw(g);
				// Update color
				similarSpring.InnerColor = color;
				// If dir not -1 update linkDirState to reflect new state
				if (dir != -1) similarSpring.LinkDirState = dir;
				// Otherwise reset linkDirState to reflect linkDir in its state
				// Note: linkDir is the actual dir, linkDirState is the current state.
				else similarSpring.ResetLinkDirState();
				// Draw spring
				similarSpring.Draw(g);
			}
			else throw new ArgumentException("Given edge not present in graph!");
		}
		public void DrawSprings()
		{
			// Draw all springs
			foreach (Spring spring in springs) spring.Draw(g);
		}
		public void UndrawSprings()
		{
			// Undraw all springs
			foreach (Spring spring in springs) spring.Undraw(g);
		}
		public void RedrawSprings()
		{
			UndrawSprings();
			DrawSprings();
		}
		public void ResetSpringColor(Edge edge)
		{
			// Find the spring representing the edge; the similar spring.
			int similarSpringIdx = FindSimillarSpringIdx(ToSpring(edge));
			// Found a similar spring - reset its color scheme
			if (similarSpringIdx != -1) springs[similarSpringIdx].SetDefaultColors();
			// Otherwise throw
			else throw new ArgumentException("Given edge not present in graph!");
		}
		public void ResetSpringColors()
		{
			foreach (var spring in springs)
			{
				spring.SetDefaultColors();
				spring.Draw(g);
			}
		}
		#endregion

		#region Helper methods for panel click events
		public Particle GetClickedParticle(float x, float y)
		{
			// If the given coordinates are within a particle will return
			// a reference to that particle, otherwise returns null
			foreach (Particle particle in particles)
				if (particle.PointIsWithin(x, y)) return particle;
			return null;
		}
		public void ToggleParticlePin(int id)
		{
			// Toggle the ping status for a given particle
			((Particle)nodeLookup[id]).TogglePin();
		}
		public void PinAllParticles()
		{
			// Pin every particle in the particle list
			foreach (Particle particle in particles) particle.Pinned = true;
		}
		public void UnpinAllParticles()
		{
			// Unpin every particle in the particle list
			foreach (Particle particle in particles) particle.Pinned = false;
		}
		#endregion

		public void Sleep(int millis)
		{
			// Helper method to use Thread.Sleep() for the given ammount of
			// milliseconds multiplied by the delay factor (1 by default)

			// If not during a pause event wait X millis
			if (!Paused) Thread.Sleep((int)Math.Round(delayFactor * millis));

			// Check for pause event
			CheckForPause();
		}
	}
}
