﻿using System;
using System.Collections.Generic;
using System.Drawing;

using AlgorithmVisualizer.GraphTheory.Utils;
using AlgorithmVisualizer.MathUtils;

namespace AlgorithmVisualizer.GraphTheory.FDGV
{
	public class Particle : GNode
	{
		// Default particle color scheme
		private static readonly Color
			defaultInnerColor = ColorTranslator.FromHtml("#646464"),
			defaultBorderColor = ColorTranslator.FromHtml("#E8E8E8"),
			defaultTextColor = ColorTranslator.FromHtml("#E8E8E8");
		private Color innerColor, borderColor, textColor;
		public Color InnerColor { set { innerColor = value; } }
		public Color BorderColor { set { borderColor = value; } }
		public Color TextColor { set { textColor = value; } }
		public void SetDefaultColors()
		{
			// Set particle's colors to defaults
			innerColor = defaultInnerColor;
			borderColor = defaultBorderColor;
			textColor = defaultTextColor;
		}

		// Used to pin the particle in its position (position will not be updated)
		private bool pinned;

		private Vector pos, vel, acc;
		// Helper method to find center pos of the panel. static becuase all particles are
		// assumed to be in the same panel
		private static Vector findCenterPos() => new Vector(panelWidth / 2, panelHeight / 2);
		// Position vector of the center of the panel that pulls all particles
		private static Vector centerPos = findCenterPos();
		// Note: size is the diameter of the particle, divided by 2 is also the raius
		private int size;
		private static int panelHeight, panelWidth;

		// Physics related & particle stabilization
		private const float G = 1000f, MAX_PARTICLE_SPEED = 7f, MIN_REPEL_MAG = 0.003f,
			MAX_REPEL_MAG = 1, MAX_CENTER_PULL_MAG = 0.5f, VEL_DECAY = 0.99f;

		// Used to capture the maximal velocity magnitude per iteration.
		// Needs to be manually reset, meaning that after an iteration over all springs/particles
		// _MAX_VEL_MAG_PER_ITR needs to be reset to 0 manually
		private static float _MAX_VEL_MAG_PER_ITR = 0;

		// Getters and setters
		// Note that vector getter/setters use a copy constructor!
		// Also when setting PanelHeight/PanelWidth centerPost will update!
		public static int PanelHeight { set { panelHeight = value; centerPos = findCenterPos(); } }
		public static int PanelWidth { set { panelWidth = value; centerPos = findCenterPos(); } }
		public static float MAX_VEL_MAG_PER_ITR { get { return _MAX_VEL_MAG_PER_ITR; } set { _MAX_VEL_MAG_PER_ITR = value; } }
		public Vector Pos { get { return new Vector(pos.X, pos.Y); } set { pos = new Vector(value.X, value.Y); } }
		public Vector Vel { get { return new Vector(vel.X, vel.Y); } set { vel = new Vector(value.X, value.Y); } }
		public Vector Acc { get { return new Vector(acc.X, acc.Y); } set { acc = new Vector(value.X, value.Y); } }
		public int Size { get { return size; } set { size = value; } }
		public bool Pinned { get { return pinned; } set { pinned = value; } }
		public void TogglePin() => pinned = !pinned;

		private static readonly StringFormat sf = new StringFormat();
		private static readonly Font font = new Font("Arial", 10);

		public Particle(int id, int data, Vector _pos, int _size) : base(id, data)
		{
			pos = new Vector(_pos.X, _pos.Y);
			vel = new Vector(0, 0);
			acc = new Vector(0, 0);
			size = _size;
			pinned = false;
			// Used to center string with sf
			sf.LineAlignment = StringAlignment.Center;
			sf.Alignment = StringAlignment.Center;
			// Use default color scheme
			SetDefaultColors();
		}

		public void Draw(Graphics g)
		{
			// Draw the particle
			using (var innerBrush = new SolidBrush(innerColor)) Draw(g, innerBrush);
		}
		public void Undraw(Graphics g)
		{
			// Undraw the particle
			using (var undrawBrush = new SolidBrush(Colors.Undraw)) Draw(g, undrawBrush);
		}
		private void Draw(Graphics g, SolidBrush brush)
		{
			int radius = size / 2;
			// Create rectagle centered around x, y
			RectangleF rect = new RectangleF(pos.X - radius, pos.Y - radius, size, size);
			// Draw/Undraw particle
			g.FillEllipse(brush, rect);
			// If not undrawing the particle
			if (brush.Color != Colors.Undraw)
			{
				// Draw border
				using (var pen = new Pen(borderColor, 1.7f)) g.DrawEllipse(pen, rect);
				// Draw node id centered within particle
				using (var textBrush = new SolidBrush(textColor))
					g.DrawString(id.ToString(), font, textBrush, rect, sf);
			}
		}

		// Add given force to acc. Note: F = ma --> a = F/m --> F = a (because m = 1) 
		public void Accelerate(Vector F) => acc += F;
		public void UpdatePos()
		{
			// Ignore pinned particles
			if (!pinned)
			{
				// Update the velocity via current acceleration
				vel += acc;
				// recude vel by some %
				vel *= VEL_DECAY;
				if (vel.Magnitude() > MAX_PARTICLE_SPEED) vel.SetMagnitude(MAX_PARTICLE_SPEED);
				// Update the position by adding the velocity to the current position
				pos += vel;
				BoundWithinPanel();
				// Keep track of maximal verlocity per iteration of the FDGV; needs to be
				// manually reset from within the FDGV algo after each iteration.
				if (vel.Magnitude() > _MAX_VEL_MAG_PER_ITR) _MAX_VEL_MAG_PER_ITR = vel.Magnitude();
			}
			// Reset the acceleration to 0 for the next iteration, otherwise paricle
			// will accelerate with each iteration and eventually "fly" of the screen.
			acc.Set(0, 0);
		}
		public void BoundWithinPanel()
		{
			// Bounds the position vector of this particle within the panel.
			// The bounding is with respect to the middle point where x, y are
			// always offset by radius from all 4 directions

			int radius = size / 2;
			// Bound X within panelWidth
			pos.X = Math.Max(radius, Math.Min(panelWidth - radius, pos.X));
			// Bound Y within panelHeight
			pos.Y = Math.Max(radius, Math.Min(panelHeight - radius, pos.Y));
		}
		public void PullToCenter()
		{
			// Vector of full length from pos to centerPos
			Vector F = centerPos - pos;
			// Compute a magnitude and adjust F's mag accordingly
			float mag = Math.Min(F.Magnitude() / G, MAX_CENTER_PULL_MAG);
			F.SetMagnitude(mag);
			// Add force into acc
			acc += F;
		}
		public void ApplyRepulsiveForces(List<Particle> paricleList)
		{
			// Gravitational force formula: F = G * (m1 + m2) / d^2

			// foreach particle in the particle list
			foreach (Particle particle in paricleList)
			{
				// If not this particle
				if (this != particle)
				{
					// Get vector of full magnitude from this.pos to particle.pos
					Vector F = particle.pos - this.pos;
					// If the magnitude is 0 (particle overlap) - randomize F
					if (F.Magnitude() == 0) F = Vector.GetRandom();
					// Bound and magnitude within [MIN_REPEL_MAG, MAX_REPEL_MAG]
					float mag = Math.Max(MIN_REPEL_MAG, Math.Min(G / (F.Magnitude() * F.Magnitude()), MAX_REPEL_MAG));
					F.SetMagnitude(mag);
					// Accelerate particle using computed force
					particle.acc += F;
				}
			}
		}

		public override string ToString()
		{
			return string.Format("ID: {0}, POS: ({1},{2})", id, pos.X, pos.Y);
		}

		public bool PointIsWithin(float x, float y)
		{
			// Check if the given point's coordinates are within the particle(circle)
			// Note: given the center point of a circle and its radius, a given point is
			// within the circle if: (x1-x2)^2 + (y1-y2)^2 <= r^2

			// Compute particle's radius
			float r = size / 2;
			// Find particle's center point
			float xc = pos.X, yc = pos.Y;
			// Use formula to check if given point (x, y) is within the particle
			float dx = xc - x, dy = yc - y;
			return dx * dx + dy * dy <= r * r;
		}
	}
}

