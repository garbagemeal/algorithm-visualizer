﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using AlgorithmVisualizer.GraphTheory.Utils;
using AlgorithmVisualizer.MathUtils;

namespace AlgorithmVisualizer.GraphTheory.FDGV
{
	public class Spring : Edge
	{
		// Default spring color scheme
		private static readonly Color
			defaultInnerColor = ColorTranslator.FromHtml("#E8E8E8"),
			defaultTextColor = ColorTranslator.FromHtml("#FF0000");
		private Color innerColor, textColor;
		public Color InnerColor { set { innerColor = value; } }
		public Color TextColor { set { textColor = value; } }
		public void SetDefaultColors()
		{
			// Set edge's colors to defaults
			innerColor = defaultInnerColor;
			textColor = defaultTextColor;
		}

		private float restingLen, k = 0.005f;
		private const float MAX_FORCE = 1f;
		private Particle p1, p2;
		public Particle P1 { get { return p1; } }
		public Particle P2 { get { return p2; } }

		/* linkDir: represents the actual direction of the original edge(s) in the graph
		 * *******************************************************************************
		 * linkDirState: represents the current state for the linkDir and not necessarily 
		 * the actual direction of the edge.
		 * *******************************************************************************
		 * Possible values and thier meaning:
		 * 0: p1 ---> p2
		 * 1: p1 <--- p2
		 * 2: p1 <--> p2 */
		private int linkDir, linkDirState;
		// Note that linkDirState changes with linkDir to reflect edits/removals of edges
		public int LinkDir { get { return linkDir; } set { linkDir = linkDirState = value; } }
		public int LinkDirState { get { return linkDirState; } set { linkDirState = value; } }
		public void ResetLinkDirState() => linkDirState = linkDir;

		public Spring(Particle _p1, Particle _p2, int cost, float _restingLen, int _linkDir) : base(_p1.Id, _p2.Id, cost)
		{
			// Set spring's attr
			restingLen = _restingLen;
			p1 = _p1;
			p2 = _p2;
			linkDir = linkDirState = _linkDir;
			// Use default color scheme
			SetDefaultColors();
		}

		public void ExertForcesOnParticles()
		{
			// Method to apply forces on the spring's composing particles
			Vector F = p2.Pos - p1.Pos;
			// Magnitude limited via MAX_FORCE
			float mag = Math.Min(MAX_FORCE, k * (F.Magnitude() - restingLen));
			F.SetMagnitude(mag);
			p1.Accelerate(F);
			//F *= -1;
			p2.Accelerate(F * -1);
		}
		public void Draw(Graphics g)
		{
			// Draw the spring
			using (var innerBrush = new SolidBrush(innerColor)) Draw(g, innerBrush);
		}
		public void Undraw(Graphics g)
		{
			// Undraw the spring
			using (var undrawBrush = new SolidBrush(Colors.Undraw)) Draw(g, undrawBrush);
		}
		private void Draw(Graphics g, SolidBrush brush)
		{
			// Draw this spring using the given brush

			// center points of the particles
			int p1Rad = p1.Size / 2, p2Rad = p2.Size / 2;
			PointF pt1 = new PointF(p1.Pos.X, p1.Pos.Y);
			PointF pt2 = new PointF(p2.Pos.X, p2.Pos.Y);

			// Offsetting the line starting/ending pos on the particle borders
			Vector vector = p2.Pos - p1.Pos;
			vector.SetMagnitude(p1Rad);
			pt1.X += vector.X;
			pt1.Y += vector.Y;
			vector.SetMagnitude(p2Rad);
			pt2.X -= vector.X;
			pt2.Y -= vector.Y;

			// Drawing the line between pt1, pt2
			using (Pen pen = new Pen(brush, 2.5f))
			{
				using (AdjustableArrowCap bigArrow = new AdjustableArrowCap(2.7f, 2.7f))
				{
					// Note that linkDirState is used to reflect the state of the spring!
					if (linkDirState == 2 || linkDirState == 1) pen.CustomStartCap = bigArrow;
					if (linkDirState == 2 || linkDirState == 0) pen.CustomEndCap = bigArrow;
					g.DrawLine(pen, pt1, pt2);
				}
			}

			// Find center point of vector from p1 to p2
			vector = (p2.Pos - p1.Pos) / 2 + p1.Pos;

			// if selected brush is not the undrawing brush
			if (brush.Color != Colors.Undraw) brush = new SolidBrush(textColor);

			// Draw the cost of the edge on the line, centered
			using (Font font = new Font("Arial", 11, FontStyle.Bold))
			{
				using (StringFormat sf = new StringFormat())
				{
					sf.LineAlignment = StringAlignment.Center;
					sf.Alignment = StringAlignment.Center;
					g.DrawString(cost.ToString(), font, brush, vector.X, vector.Y, sf);
				}
			}
			// Dispose of brush in case a new 1 was created in if statement
			brush.Dispose();
		}

		public bool ContainsID(int id)
		{
			// Check if the given id is p1's or p2's id
			return p1.Id == id || p2.Id == id;
		}
		public bool SimilarTo(Spring s)
		{
			// Check if given spring is "Similar" to this spring
			// 2 springs are similar if the composing particles are the same
			// in any order
			return p1 == s.p1 && p2 == s.p2 || p1 == s.p2 && p2 == s.p1;
		}

		//public override string ToString()
		//{
		//	return string.Format("P1: {0},\nP2: {1}\n", p1, p2);
		//}
		//public override string ToString()
		//{
		//	return base.ToString() + " Link dir: " + LinkDir.ToString();
		//}
	}
}
