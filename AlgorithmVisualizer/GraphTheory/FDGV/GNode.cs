﻿using System;

namespace AlgorithmVisualizer.GraphTheory.FDGV
{
	public class GNode : IComparable
	{
		protected int id;
		private int data;
		// When using the setter for id make sure the id is not already in use
		// and make sure to update nodeLookup, adjList, and both the incoming
		// and outgoing edges for this node!
		public int Id { get { return id; } set { id = value; } }
		public int Data { get { return data; } set { data = value; } }

		public GNode(int _id, int _data)
		{
			id = _id;
			data = _data;
		}

		public override bool Equals(object obj)
		{
			// returns true if the given object is a GNode and has the same id
			return obj is GNode && id == ((GNode)obj).Id;
		}
		public int CompareTo(object obj)
		{
			// returns the difference of this node's data with the given
			// obj is assumed to be of type GNode!
			return data - ((GNode)obj).Data;
		}
		public static bool operator ==(GNode gn1, GNode gn2)
		{
			// true if either both gn1 and gn2 are objects that share the same id
			// or nither of them is an object(both are null)
			// false otherwise
			return gn1 is object && gn2 is object && gn1.Id == gn2.Id
				|| !(gn1 is object || gn2 is object);
		}
		public static bool operator !=(GNode gn1, GNode gn2)
		{
			// true if both gn1 and gn2 are objects (not null) but do not share the same id,
			// or if exactly one of gn1 or gn2 is null (not an object), flase otherwise.
			return gn1 is object && gn2 is object && gn1.Id != gn2.Id
				|| gn1 is object && !(gn2 is object) || gn2 is object && !(gn1 is object);
		}

		public override string ToString()
		{
			return string.Format($"id: {id}, val: {data}");
		}
	}
}

