﻿using System.Collections.Generic;
using System.Drawing;

namespace AlgorithmVisualizer.GraphTheory.FDGV
{
	public class GComponent
	{
		// A class to represent a component in a graph.
		// used for the DFS based connected componenets algo
		private int id;
		private static int componentCount = 0;
		private static readonly Color[] colors = { Color.Red, Color.Lime, Color.White,
			Color.Blue, Color.Purple, Color.Orange, Color.Gold };
		private List<int> nodeIds;

		public int Id { get { return id; } set { id = value; } }
		public int ComponentCount { get { return componentCount; } }
		public Color Color { get { return colors[id]; } }
		public List<int> NodeIds { get { return nodeIds; } }

		public GComponent()
		{
			id = componentCount++;
			nodeIds = new List<int>();
		}
		public override string ToString()
		{
			string nodeIdsStr = "";
			foreach (int nodeId in nodeIds) nodeIdsStr += nodeId + ", ";
			// Note that % colors.Length is used to prevent out of bounds errors
			return string.Format("Component[ID: {0}, Color: {1}, node ids: {2}]",
				id, colors[id % colors.Length], nodeIdsStr);
		}
	}
}

