﻿namespace AlgorithmVisualizer.GraphTheory.FDGV
{
	public interface IGraph
	{
		// This interface defines signatures for basic methods
		// for a graph implementation, namely a directed graph.
		bool AddNode(int id, int data);
		bool RemNode(int id);
		bool SLinkNodes(int srcId, int dstId, int cost);
		bool DLinkNodes(int srcId, int dstId, int cost);
		bool SUnlinkNodes(int from, int to, int cost);
		bool DUnlinkNodes(int from, int to, int cost);
	}
}
