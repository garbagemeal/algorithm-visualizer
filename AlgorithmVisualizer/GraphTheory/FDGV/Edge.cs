﻿using System;

namespace AlgorithmVisualizer.GraphTheory.FDGV
{
	public class Edge : IComparable
	{
		private int from, to;
		protected int cost;
		public int From { get { return from; } set { from = value; } }
		public int To { get { return to; } set { to = value; } }
		public int Cost { get { return cost; } set { cost = value; } }

		public Edge(int _from, int _to, int _cost)
		{
			from = _from;
			to = _to;
			cost = _cost;
		}

		public static Edge Reversed(Edge edge)
		{
			// Returns a new edge where the from/to id's are swapped
			return new Edge(edge.to, edge.from, edge.cost);
		}

		// Note: comparison of edges is done only via the from and to fields!
		// Reason: prohibit 2 edges of different costs from A to B
		public override bool Equals(object obj)
		{
			// true if obj is an Edge and from/to are equal in the same order
			// cost is ignored
			return obj is Edge && from == ((Edge)obj).from && to == ((Edge)obj).to;
		}
		public static bool operator ==(Edge e1, Edge e2)
		{
			// Returns true if e1 and e2 are objects (non null) and share the same from/to ids
			// or if both e1 and e2 are null, false otherwise.
			return e1 is object && e2 is object && e1.from == e2.from && e1.to == e2.to
				|| !(e1 is object || e2 is object);
		}
		public static bool operator !=(Edge e1, Edge e2)
		{
			// Returns true if e1 and e2 are objects (non null) and dont share the same from/to ids
			// or if exactly one of e1 or e2 is null, false otherwise.
			return e1 is object && e2 is object && (e1.from != e2.from || e1.to != e2.to)
				|| e1 is object && !(e2 is object) || e2 is object && !(e1 is object);
		}
		public int CompareTo(object obj)
		{
			/*
			 * Note: CompareTo only compares the edge costs, unlinke Equals or operator overrides
			 * also note that here obj is assumed to be Spring! if not then an exception will be raised!
			 * */
			return cost - ((Edge)obj).cost;
		}

		public override string ToString()
		{
			return string.Format("({0}, {1}, {2})", from, to, cost);
		}
	}
}
