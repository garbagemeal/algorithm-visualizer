﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

using AlgorithmVisualizer.DBHandler;
using AlgorithmVisualizer.Forms.FDGV;
using AlgorithmVisualizer.MathUtils;

namespace AlgorithmVisualizer.GraphTheory.FDGV
{
	public class GraphAdjList : ForceDirectedGraphVisualizer, IGraph
	{
		// Directed adjacency list representation of a graph.

		// The adjacencey list
		private Dictionary<int, List<Edge>> adjList;
		public Dictionary<int, List<Edge>> AdjList { get { return adjList; } }
		private int edgeCount;
		public int NodeCount { get { return nodeLookup.Count; } }
		public int EdgeCount { get { return edgeCount; } }

		public GraphAdjList(Graphics gMain, Graphics gLog, int panelHeight, int panelWidth)
			: base(gMain, gLog, panelHeight, panelWidth)
		{
			adjList = new Dictionary<int, List<Edge>>();
			edgeCount = 0;
		}

		#region Operations on graph
		public bool ContainsNode(int id) => nodeLookup.ContainsKey(id);

		public bool AddNode(int id, int data)
		{
			return AddNode(id, data, GetRndPosVector());
		}
		public bool AddNode(int id, int data, Vector posVector)
		{
			if (ContainsNode(id)) return false;
			Particle particle = new Particle(id, data, posVector, PARTICLE_SIZE);
			// particle automatically downcasted to a GNode
			nodeLookup[id] = particle;
			adjList[id] = new List<Edge>();
			// For visualization:
			AddParticle(particle);
			return true;
		}
		public bool RemNode(int id)
		{
			if (!ContainsNode(id)) return false;
			// Removing all edges that connect to the node with given id
			// Foreach edge connected to the node with the given id
			foreach (Edge edge in adjList[id])
			{
				// Get destination node's(to's) edge list, iterate thru it
				List<Edge> edgeList = adjList[edge.To];
				for (int i = 0; i < edgeList.Count;)
				{
					// current edge links back to the node for removal (To == given id) remove it
					if (edgeList[i].To == id)
					{
						edgeList.RemoveAt(i);
						edgeCount--;
					}
					else i++;
				}
			}
			// Remove the edge list altogether for the node for removal
			edgeCount -= adjList[id].Count;
			adjList.Remove(id);
			// Visualization (Removal of the particle and also all springs connected):
			RemParticle(id);
			// Remove entry in nodeLookup (can only be done after RemParticle as it uses nodeLookup)
			nodeLookup.Remove(id);
			return true;
		}

		public bool SLinkNodes(int from, int to, int cost)
		{
			// Both node id's present in nodeLookup and not trying to create a self loop
			if (to != from && ContainsNode(from) && ContainsNode(to))
			{
				// Creating the edge for addition (as a spring)
				Particle p1 = (Particle)nodeLookup[from], p2 = (Particle)nodeLookup[to];
				Spring spring = new Spring(p1, p2, cost, SPRING_REST_LEN, 0);
				// If the adjList does not already contain the same edge (even of different cost)
				if (!adjList[from].Contains(spring))
				{
					// Creating a reversed edge from the spring
					Edge revEdge = Edge.Reversed(spring);
					// to's edge list
					List<Edge> toEdgeList = adjList[to];
					// Check if adding the edge retults in a double link of different costs,
					// if this is the case the spring will NOT be added!
					for (int i = 0; i < toEdgeList.Count; i++)
						if (toEdgeList[i] == revEdge && toEdgeList[i].Cost != revEdge.Cost) return false;
					adjList[from].Add(spring);
					// For visualization
					AddSpring(spring);
					edgeCount++;
					return true;
				}
			}
			return false;
		}
		public bool DLinkNodes(int from, int to, int cost)
		{
			// Both node id's present in nodeLookup and not trying to create a self loop
			if (to != from && ContainsNode(from) && ContainsNode(to))
			{
				// Creating both edges for addition: (u, v, x), (v, u, x)
				// The first edge is created as a Spring extending the edge for the visualization
				Particle p1 = (Particle)nodeLookup[from], p2 = (Particle)nodeLookup[to];
				Spring spring = new Spring(p1, p2, cost, SPRING_REST_LEN, 2);
				Edge revEdge = Edge.Reversed(spring);
				// Prohibit More then 1 edge from A to B and vise versa
				if (!adjList[from].Contains(spring) && !adjList[to].Contains(revEdge))
				{
					adjList[from].Add(spring);
					adjList[to].Add(revEdge);
					// Visualization (Adding the doubly linked spring):
					AddSpring(spring);
					edgeCount += 2;
					return true;
				}
			}
			return false;
		}
		public bool SUnlinkNodes(int from, int to, int cost)
		{
			// Removes specified edge, removes only 1 link if edge was doubly linked

			// Both node id's present in nodeLookup and not trying to remove a self loop
			if (to != from && ContainsNode(from) && ContainsNode(to))
			{
				// Creating a clone of the edge for removal, getting from's edge list
				Edge edgeToRem = new Edge(from, to, cost);
				List<Edge> fromEdgeList = adjList[from];
				// for each edge in fromEdgeList
				for (int i = 0; i < fromEdgeList.Count; i++)
				{
					// Cuurent edge is the same as the clone edge
					if (fromEdgeList[i] == edgeToRem)
					{
						// remove edge from edge list
						fromEdgeList.RemoveAt(i);
						// Visualization (Removal/Modification of the spring):
						RemOrModSpring(edgeToRem);
						edgeCount--;
						return true;
					}
				}
			}
			return false;
		}
		public bool DUnlinkNodes(int from, int to, int cost)
		{
			// Removes specified edge only if found to be doubly linked

			// Both node id's present in nodeLookup and not trying to remove a self loop
			if (to != from && ContainsNode(from) && ContainsNode(to))
			{
				Edge edgeToRem = new Edge(from, to, cost);
				List<Edge> fromEdgeList = adjList[from], toEdgeList = adjList[to];
				// Find the edge (from, to, cost)
				for (int i = 0; i < fromEdgeList.Count; i++)
				{
					if (fromEdgeList[i] == edgeToRem)
					{
						// Find the edge (to, from, cost)
						Edge revEdgeToRem = Edge.Reversed(edgeToRem);
						for (int j = 0; j < toEdgeList.Count; j++)
						{
							if (toEdgeList[j] == revEdgeToRem)
							{
								// Removal of both edges, not that only both can be removed or none at all
								fromEdgeList.RemoveAt(i);
								toEdgeList.RemoveAt(j);
								edgeCount -= 2;
								// second argument indicates a DLinked removal for the FDGV
								RemOrModSpring(edgeToRem, true);
								return true;
							}
						}
					}
				}
			}
			return false;
		}

		public void FixNodeIdNumbering()
		{
			// Method to fix the node id numbering in case
			// it is not sequential starting from 0.
			// Total runtime: O(Vlog(V) + E) = O(Vlog(V) + V^2) = O(V^2)

			// Create a sorted array containing all node ids
			// O(V)
			int[] nodeIdArray = nodeLookup.Keys.ToArray();
			// Assumed to take O(VLog(V)) time
			Array.Sort(nodeIdArray);
			int N = nodeIdArray.Length;
			// Array values are not sequntial starting from 0
			if(nodeIdArray[0] > 0 || nodeIdArray[N - 1] - nodeIdArray[0] > N - 1)
			{
				// Note that each index denotes the new node id,
				// and the value denotes the old node id.

				// inverseNodeIdDict - inverse mapping for nodeIdArray, where
				// keys (old node ids) map to the new node id.
				// A dictionary is used becuase nodeIdArray's values are not sequential
				Dictionary<int, int> inverseNodeIdDict = new Dictionary<int, int>();

				// O(V + E)
				UpdateNodeIds(nodeIdArray, N, inverseNodeIdDict);

				// redraw graph
				DrawGraphForceless();
			}
		}
		private void UpdateNodeIds(int[] nodeIdArray, int N, Dictionary<int, int> inverseNodeIdDict)
		{
			// O(V)
			// go over each node id in nodeIdArray
			Console.WriteLine("List of updated node ids:");
			for (int i = 0; i < N; i++)
			{
				int newId = i, oldId = nodeIdArray[i];
				inverseNodeIdDict[oldId] = newId;
				if (newId != oldId)
				{
					// Update nodeLookup
					nodeLookup[newId] = nodeLookup[oldId];
					nodeLookup.Remove(oldId);
					// Update adjList
					adjList[newId] = adjList[oldId];
					adjList.Remove(oldId);
					// Update node id (GNode instance)
					nodeLookup[newId].Id = newId;
					// Output some info for the user
					Console.WriteLine($"oldID: {oldId}, newID: {newId}");
				}
			}
			// O(E)
			UpdateNodeIdsInEdges(inverseNodeIdDict);
		}
		private void UpdateNodeIdsInEdges(Dictionary<int, int> inverseNodeIdDict)
		{
			// O(E)
			// Update all node ids (from/to) for each edge using inverseNodeIdDict
			foreach (List<Edge> edgeList in adjList.Values)
			{
				foreach (Edge edge in edgeList)
				{
					edge.From = inverseNodeIdDict[edge.From];
					edge.To = inverseNodeIdDict[edge.To];
				}
			}
		}

		public void ClearGraph()
		{
			// Clear the graph (logically and visually)
			ClearVisualization();
			nodeLookup.Clear();
			adjList.Clear();
			edgeCount = 0;
		}
		#endregion

		#region Serialization and deserialization a graph
		public void Serialize(string name)
		{
			// Serialize this graph into a string (adjacency list)

			FixNodeIdNumbering();
			string serialization = "";
			for (int i = 0; i < NodeCount; i++)
			{
				string row = i + ": ";
				foreach (Edge edge in adjList[i]) row += $"({edge.To},{edge.Cost}), ";
				serialization += row + "\n";
			}
			// Save preset in DB
			DBConnection db = DBConnection.GetInstance();
			if (db.Connect())
			{
				// Connect to DB and store new preset with given name and disconnect
				var newPreset = new Preset(name, serialization);
				if (!db.AddPreset(newPreset))
					Console.WriteLine($"Failed to add preset '{name}'");
				db.Disconnect();
			}
		}
		public void Deserialize(string[] serialization)
		{
			// Given the serialization of a graph as a string array will deserialize it, 
			// that is recreate the graph by parsing it.
			// Note that first all nodes must be added, and only after also the edges.(an
			// edge may link to a node that has not yet been created)

			if (serialization == null) throw new ArgumentException("Serial may not be null!");
			ClearGraph();
			// Parsing vertices from serialization
			foreach (string line in serialization)
			{
				if (!String.IsNullOrEmpty(line))
				{
					// Split entire line via ": "
					string[] idAndEdgesSplit = Split(line, ": ");
					int from = Int32.Parse(idAndEdgesSplit[0]);
					// Add the node into the graph
					AddNode(from, 0);
				}
			}
			// Parsing edges from serialization
			foreach (string line in serialization)
			{
				if (!String.IsNullOrEmpty(line))
				{
					// Split entire line via ": "
					string[] splitIdAndEdges = Split(line, ": ");
					int from = Int32.Parse(splitIdAndEdges[0]);
					// Split edges list via ", "
					string[] edgesAsStr = Split(splitIdAndEdges[1], ", ");
					if (edgesAsStr != null)
					{
						foreach (string edgeAsStr in edgesAsStr)
						{
							if (!String.IsNullOrEmpty(edgeAsStr))
							{
								// Remove () from str
								string edgeWithoutParenthesis = edgeAsStr.Substring(1, edgeAsStr.Length - 2);
								// Split edge via ',', first split is the dst node second split is the cost
								string[] splitToAndCost = edgeWithoutParenthesis.Split(',');
								int to = Int32.Parse(splitToAndCost[0]), cost = Int32.Parse(splitToAndCost[1]);
								// Add the edge into the graph
								SLinkNodes(from, to, cost);
							}
						}
					}
				}
			}

			// Helper method to split a given string using a given splitter(string).
			string[] Split(string str, string splitter) =>
				str.Split(new string[] { splitter }, StringSplitOptions.None);
		}
		#endregion

		#region Miscellaneous
		public int GetRandomNodeId()
		{
			// Returns a random key from the nodeLookup - a random node id
			return nodeLookup.ElementAt(rnd.Next(nodeLookup.Count)).Key;
		}
		public Dictionary<int, List<Edge>> GetGTranspose()
		{
			// Returns a new adjacency list, Gt, where each edge(u, v, x) becomes(v, u, x)
			Dictionary<int, List<Edge>> Gt = new Dictionary<int, List<Edge>>();
			for (int id = 0; id < NodeCount; id++) Gt[id] = new List<Edge>();

			for (int id = 0; id < NodeCount; id++)
			{
				foreach (Edge edge in adjList[id])
				{
					// Compute reversed edge, store in Gt
					Edge revEdge = Edge.Reversed(edge);
					Gt[revEdge.From].Add(revEdge);
				}
			}
			return Gt;
		}
		public List<Edge> GetUndirectedEdgeList()
		{
			// *** SHOULD ONLY BE USED FOR UNDIRECTED GRAPHS! ***

			// O(E)
			// Assumes the graph is "undirected"; for each edge (u, v, x)
			// there exists an edge (v, u, x).
			// Returns a list of all edges(u, v, x) in the graph where u < v
			List<Edge> edgeList = new List<Edge>();
			foreach (List<Edge> curEdgeList in adjList.Values)
			{
				if (curEdgeList != null)
					foreach (Edge edge in curEdgeList)
						if (edge.From < edge.To) edgeList.Add(edge);

			}
			return edgeList;
		}
		public bool IsEmpty() => NodeCount == 0;
		#endregion
	}
}
