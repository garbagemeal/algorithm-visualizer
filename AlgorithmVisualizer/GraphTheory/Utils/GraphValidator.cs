﻿using System;
using System.Collections.Generic;
using AlgorithmVisualizer.GraphTheory.Algorithms;
using AlgorithmVisualizer.GraphTheory.FDGV;

namespace AlgorithmVisualizer.GraphTheory.Utils
{
	static class GraphValidator
	{
		// Util class with methods to check for some properties of the graph
		public static bool IsUndirected(GraphAdjList graph)
		{
			// Method to check if the graph is undirected
			// Go over all edges in the graph
			// if any edge does not appear reversed in adjList[edge.To]
			// return false (found a directed edge, should be undirected)
			foreach (List<Edge> edgeList in graph.AdjList.Values)
			{
				foreach (Edge edge in edgeList)
				{
					if (!graph.AdjList[edge.To].Contains(Edge.Reversed(edge)))
					{
						Console.WriteLine($"Found a directed edge: {edge}\nThe graph is not undirected!");
						return false;
					}
				}
			}
			return true;
		}
		public static bool IsConnectedUndirected(GraphAdjList graph)
		{
			// Run connected components algo on the graph without visuals and
			// check if there is only 1 component (meaning the graph is connected)
			// Note: the call to ConnectedComponentsDisjointSet ensures G is undirected.
			if (new ConnectedComponentsDisjointSet(graph, vizMode: false).ComponentCount == 1)
				return true;
			else Console.WriteLine("The graph is not connected and undirected!");
			return false;
		}
		public static bool IsDAG(GraphAdjList graph)
		{
			// Checks if the graph is a DAG using Kahn's algo
			if (new KahnsTopSort(graph, vizMode: false).TopOrder != null) return true;
			else Console.WriteLine("Graph is not a DAG!");
			return false;
		}
		public static bool IsPositiveEdgeWeighted(GraphAdjList graph)
		{
			// Checks if all the edges in the graph have a positive weight
			// this check is important for Dijkstra's algorithm!
			foreach (List<Edge> edgeList in graph.AdjList.Values)
			{
				foreach (Edge edge in edgeList)
				{
					if (edge.Cost < 0)
					{
						Console.WriteLine("The graph is not positive edge weighted!");
						return false;
					}
				}
			}
			return true;
		}
	}
}
