﻿using System;

namespace AlgorithmVisualizer.MathUtils
{
	public class Vector
	{
		// Class to represent a 2d vector
		private float x, y;
		private static Random rnd = new Random();
		public float X { get { return x; } set { x = value; } }
		public float Y { get { return y; } set { y = value; } }
		public Vector(float _x, float _y)
		{
			Set(_x, _y);
		}

		public void Set(float _x, float _y)
		{
			// Function to set both x and y
			x = _x;
			y = _y;
		}
		// Overloading arithmetic operations (+, -, *, /)
		public static Vector operator +(Vector v1, Vector v2)
		{
			return new Vector(v1.X + v2.X, v1.Y + v2.Y);
		}
		public static Vector operator -(Vector v1, Vector v2)
		{
			return new Vector(v1.X - v2.X, v1.Y - v2.Y);
		}
		public static Vector operator *(Vector v, float scalar)
		{
			return new Vector(v.X * scalar, v.Y * scalar);
		}
		public static Vector operator /(Vector v, float scalar)
		{
			if (scalar == 0) throw new DivideByZeroException("Can't divide by 0!");
			return new Vector(v.X / scalar, v.Y / scalar);
		}

		public float Magnitude()
		{
			// return distance using the pythagorean formula
			return (float)Math.Sqrt(x * x + y * y);
		}
		public void Normalize()
		{
			// Scale vector such that its magnitude becomes 1
			// do nothing if current magnitude is 0 or 1
			float m = Magnitude();
			if (m != 0 && m != 1)
			{
				x /= m;
				y /= m;
			}
		}
		public void SetMagnitude(float mag)
		{
			// Normalize the vector and then set to new mag
			Normalize();
			x *= mag;
			y *= mag;
		}
		private static float NextFloat()
		{
			// scaled to the range -1 to 1
			double mantissa = (rnd.NextDouble() * 2.0) - 1.0;
			// scalar (power of 2)
			double exponent = Math.Pow(2.0, rnd.Next(-126, 127));
			// returns the manitisa scaled (multiplied) by the exponent
			return (float)(mantissa * exponent);
		}
		public static Vector GetRandom()
		{
			// Returns a new randomized vector
			return new Vector(NextFloat(), NextFloat());
		}
		public override string ToString()
		{
			return string.Format("({0}, {1})", x, y);
		}
	}
}